# Pincode India Lookup

Easy pincode lookup for India (updated 2019)

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install pincode-lookup-india.

```bash
npm install pincode-lookup-india
```

## Usage

```node
const { lookup } = require('pincode-lookup-india')

const data = lookup(pincode)

console.log(data)

//{
//  Region: 'Southern , Madurai',
//  Division: 'Kanniyakumari',
//  Office: 'Rajakkamangalam B.O',
//  OfficeType: 'BO',
//  District: 'KANYAKUMARI',
//  State: 'Tamil Nadu',
//  Pincode: 629502
//}


```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
