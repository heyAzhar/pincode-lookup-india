const path = require("path");
const fs = require("fs");

exports.lookup = (pincode) => {
  if (!pincode) return false;

  pincode = pincode.toString();

  const filePath = path.resolve(__dirname, `./pincode-india.json`);
  const data = JSON.parse(fs.readFileSync(filePath, "utf8"));

  const foundData = data[pincode];

  if (!foundData) return false;

  return foundData;
};
